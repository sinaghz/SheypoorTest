package com.sinaghz.sheypoortest.fragments;

/**
 * Created by sina on 8/22/2017.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinaghz.sheypoortest.R;
import com.sinaghz.sheypoortest.Util;
import com.sinaghz.sheypoortest.entities.Image;
import com.squareup.picasso.Picasso;

public class MovieDetailFragment extends Fragment{

    ImageView movieImage;
    TextView summaryText;
    TextView filmName;
    TextView timeRating;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_detail, container,
                false);

        movieImage = (ImageView) view.findViewById(R.id.moviebackground);
        summaryText = (TextView) view.findViewById(R.id.summarytext);
        filmName = (TextView) view.findViewById(R.id.moviename);
        timeRating = (TextView) view.findViewById(R.id.moviedetail);

        Bundle objects = getArguments();
            if (objects.getString(Util.FILMNAME) != null){
            filmName.setText(objects.getString(Util.FILMNAME));
        }

        if (objects.getString(Util.FILMSUMMARY) != null){
            summaryText.setText(objects.getString(Util.FILMSUMMARY));
        }

        if (objects.getString(Util.FILMORIGINALIMAGE) != null){
            Picasso.with(getActivity()).load(objects.getString(Util.FILMORIGINALIMAGE)).into(movieImage);
        }

        if (objects.getString(Util.FILMDETAIL) != null){
            timeRating.setText(objects.getString(Util.FILMDETAIL));
        }




        return view;
    }
}
