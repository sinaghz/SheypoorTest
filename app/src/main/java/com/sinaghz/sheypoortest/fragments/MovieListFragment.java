package com.sinaghz.sheypoortest.fragments;

/**
 * Created by sina on 8/22/2017.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.sinaghz.sheypoortest.MainActivity;
import com.sinaghz.sheypoortest.MovieListAdapter;
import com.sinaghz.sheypoortest.R;
import com.sinaghz.sheypoortest.Util;
import com.sinaghz.sheypoortest.database.DataBaseHelper;
import com.sinaghz.sheypoortest.database.model.DbModel;
import com.sinaghz.sheypoortest.entities.Movie;
import com.sinaghz.sheypoortest.onItemClickListener;
import com.sinaghz.sheypoortest.rest.MovieService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieListFragment extends Fragment {

    View mainView;
    MainActivity mainActivity;
    List<DbModel> allMovies;
    Button del, checkall, duplicate;
    MovieListAdapter adapter;
    RecyclerView recyclerView;
    DataBaseHelper db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_movie_list, container,
                false);
        del = mainView.findViewById(R.id.delete);
        checkall = mainView.findViewById(R.id.check_all);
        duplicate = mainView.findViewById(R.id.duplicate);
        recyclerView = (RecyclerView) mainView.findViewById(R.id.recyclerview);

        db = new DataBaseHelper(getActivity());
        checkall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allMovies != null) {
                    for (DbModel movie : allMovies) {
                        movie.setChecked(true);
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        });

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allMovies != null) {
                    Iterator<DbModel> movieIterator = allMovies.iterator();
                    while (movieIterator.hasNext()){
                        DbModel movie = movieIterator.next();
                        if (movie.getChecked()) {
                            db.deleteMovie(movie);
                            movieIterator.remove();
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        });


        allMovies = db.getAllMovies();
        initRecyclerView();

        return mainView;
    }

    private void initRecyclerView() {
        if (allMovies.size() > 0) {
            adapter = new MovieListAdapter(allMovies, mainActivity, new onItemClickListener() {
                @Override
                public void onClick(int index) {
                    changeFragment(index);
                }
            });
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));

        }
    }

    void changeFragment(int index) {
        MovieDetailFragment fragment = new MovieDetailFragment();
        Bundle bundle = new Bundle();

            bundle.putString(Util.FILMNAME, allMovies.get(index).getName());

        if (Util.hasTest(allMovies.get(index).getSummary()))
            bundle.putString(Util.FILMSUMMARY, allMovies.get(index).getSummary().replaceAll("<.*?>", ""));

        if (Util.hasTest(allMovies.get(index).getOriginalurl()))
            bundle.putString(Util.FILMORIGINALIMAGE, allMovies.get(index).getOriginalurl());

        bundle.putString(Util.FILMDETAIL, allMovies.get(index).getRate());

        fragment.setArguments(bundle);
        mainActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmain, fragment).addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }


}
