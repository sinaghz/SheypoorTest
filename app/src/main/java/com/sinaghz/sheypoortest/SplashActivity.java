package com.sinaghz.sheypoortest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.sinaghz.sheypoortest.database.DataBaseHelper;
import com.sinaghz.sheypoortest.entities.Movie;
import com.sinaghz.sheypoortest.rest.MovieService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Admin on 10/28/2018.
 */

public class SplashActivity extends AppCompatActivity {

    List<Movie> allMovies;
    private DataBaseHelper db;
    private final static String MY_PREFS_NAME = "MOVIEAPP";
    private final static String RETRIEVED = "RETRIVED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Boolean retrived = prefs.getBoolean(RETRIEVED, false);
        if (retrived) {
            Intent intent = new Intent(SplashActivity.this,MainActivity.class);
            startActivity(intent);
            return;
        }


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.BASAEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        db = new DataBaseHelper(this);
        MovieService service = retrofit.create(MovieService.class);
        Call<List<Movie>> movies = service.listMovie("girls");
        movies.enqueue(new Callback<List<Movie>>() {
            @Override
            public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
                allMovies = response.body();
                for (Movie movie:allMovies){
                    db.insertMovie(movie);
                }
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean(RETRIEVED, true);
                editor.apply();

                Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<List<Movie>> call, Throwable t) {
                Log.e("Error:", t.getMessage());
                Toast.makeText(SplashActivity.this,"Cannot get movies from backend", Toast.LENGTH_LONG).show();
            }
        });
    }


}
