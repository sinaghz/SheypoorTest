package com.sinaghz.sheypoortest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinaghz.sheypoortest.database.model.DbModel;
import com.sinaghz.sheypoortest.entities.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sina on 8/22/2017.
 */
public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MyViewHolder> {

    private List<DbModel> moviesList;
    Context context;
    onItemClickListener onItemClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public TextView title;
        public CheckBox seleCheckBox;
        public View view;


        public MyViewHolder(View view) {
            super(view);
            thumbnail = view.findViewById(R.id.thumbnail);
            title = view.findViewById(R.id.title);
            seleCheckBox = view.findViewById(R.id.movie_select);
            this.setIsRecyclable(false);
            this.view = view;

        }
    }


    public MovieListAdapter(List<DbModel> moviesList, Context ctx, onItemClickListener onItemClickListener) {
        this.moviesList = moviesList;
        this.context = ctx;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_cell, parent, false);
        int height = parent.getMeasuredHeight() / 4;
        itemView.setMinimumHeight(height);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final DbModel movie = moviesList.get(position);
        if (Util.hasTest(movie.getUrl())) {
            Picasso.with(context).load(movie.getUrl()).into(holder.thumbnail);
        }
        holder.title.setText(movie.getName());


        holder.seleCheckBox.setChecked(movie.getChecked());

        holder.seleCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
                movie.setChecked(isChecked);
            }
        });

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}