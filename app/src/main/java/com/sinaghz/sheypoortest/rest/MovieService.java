package com.sinaghz.sheypoortest.rest;

import com.sinaghz.sheypoortest.entities.Movie;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by sina on 8/22/2017.
 */

public interface MovieService {

    @GET("search/shows")
    Call<List<Movie>> listMovie(@Query("q") String item);
}
