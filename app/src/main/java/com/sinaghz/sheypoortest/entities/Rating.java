package com.sinaghz.sheypoortest.entities;

/**
 * Created by sina on 8/22/2017.
 */
public class Rating {

    Double average;

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }
}
