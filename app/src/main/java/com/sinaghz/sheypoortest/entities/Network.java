package com.sinaghz.sheypoortest.entities;

/**
 * Created by sina on 8/22/2017.
 */
public class Network {

    Integer id;
    String name;
    Country country;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
