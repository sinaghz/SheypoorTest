package com.sinaghz.sheypoortest.entities;

/**
 * Created by sina on 8/22/2017.
 */
public class Image {

    String medium;
    String original;


    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
}
