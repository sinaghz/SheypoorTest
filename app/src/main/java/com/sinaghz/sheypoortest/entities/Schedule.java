package com.sinaghz.sheypoortest.entities;

import java.util.List;

/**
 * Created by sina on 8/22/2017.
 */
public class Schedule {

    String time;
    List<String> days;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<String> getDays() {
        return days;
    }

    public void setDays(List<String> days) {
        this.days = days;
    }
}
