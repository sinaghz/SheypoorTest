package com.sinaghz.sheypoortest.entities;

/**
 * Created by sina on 8/22/2017.
 */
public class Country {
    String name;
    String code;
    String timezone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
