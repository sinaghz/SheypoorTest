package com.sinaghz.sheypoortest.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sina on 8/22/2017.
 */
public class Movie implements Parcelable  {

    Double score;

    Show show;

    Boolean checked = false;

    Boolean showit = true;

    protected Movie(Parcel in) {
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public Boolean getChecked() {
        if (checked == null){
            checked = false;
        }
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Boolean getShowit() {
        if (showit == null){
            showit = true;
        }
        return showit;
    }

    public void setShowit(Boolean showit) {
        this.showit = showit;
    }
}
