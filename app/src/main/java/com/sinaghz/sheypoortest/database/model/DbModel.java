package com.sinaghz.sheypoortest.database.model;

/**
 * Created by Admin on 10/28/2018.
 */

public class DbModel {

    public static final String TABLE_NAME = "movies";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_URL = "url";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_SUMMARY = "summary";
    public static final String COLUMN_ORIGINALURL = "originalurl";
    public static final String COLUMN_RATE = "rate";

    private int id;
    private String url;
    private String name;
    private String summary;
    private String originalurl;
    private String rate;
    Boolean checked = false;

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_URL + " TEXT,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_SUMMARY + " TEXT,"
                    + COLUMN_ORIGINALURL + " TEXT,"
                    + COLUMN_RATE + " TEXT"

                    + ")";


    public DbModel() {
    }


    public DbModel(int id, String url, String name, String summary, String originalurl, String rate) {
        this.id = id;
        this.url = url;
        this.name = name;
        this.summary = summary;
        this.originalurl = originalurl;
        this.rate = rate;
    }

    public DbModel(String url, String name, String summary, String originalurl, String rate) {
        this.url = url;
        this.name = name;
        this.summary = summary;
        this.originalurl = originalurl;
        this.rate = rate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getOriginalurl() {
        return originalurl;
    }

    public void setOriginalurl(String originalurl) {
        this.originalurl = originalurl;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
}
