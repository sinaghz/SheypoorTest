package com.sinaghz.sheypoortest.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.sinaghz.sheypoortest.database.model.DbModel;
import com.sinaghz.sheypoortest.entities.Movie;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 10/28/2018.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "movies_db";


    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(DbModel.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + DbModel.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public long insertMovie(Movie movie) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        if (movie.getShow() != null) {
            values.put(DbModel.COLUMN_NAME, movie.getShow().getName());
            if (movie.getShow().getImage() != null) {
                values.put(DbModel.COLUMN_ORIGINALURL, movie.getShow().getImage().getOriginal());
                values.put(DbModel.COLUMN_URL, movie.getShow().getImage().getMedium());
            }
            StringBuilder rating = new StringBuilder();
            if (movie.getShow().getRuntime() != null) {
                rating.append(movie.getShow().getRuntime());
                rating.append(" min");
                rating.append(" - ");
            }
            if (movie.getShow().getRating() != null && movie.getShow().getRating().getAverage() != null) {
                rating.append(movie.getShow().getRating().getAverage());
                rating.append(" /10");
            }
            values.put(DbModel.COLUMN_RATE, rating.toString());
            if (movie.getShow().getSummary() != null) {
                values.put(DbModel.COLUMN_SUMMARY, movie.getShow().getSummary().replaceAll("<.*?>", ""));
            }
        }
        // insert row
        long id = db.insert(DbModel.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public long insertMovie(DbModel movie) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(DbModel.COLUMN_NAME, movie.getName());
        values.put(DbModel.COLUMN_ORIGINALURL, movie.getOriginalurl());
        values.put(DbModel.COLUMN_URL, movie.getUrl());
        values.put(DbModel.COLUMN_RATE, movie.getRate());
        values.put(DbModel.COLUMN_SUMMARY, movie.getSummary());

        // insert row
        long id = db.insert(DbModel.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public DbModel getMovie(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(DbModel.TABLE_NAME,
                new String[]{DbModel.COLUMN_ID, DbModel.COLUMN_NAME, DbModel.COLUMN_ORIGINALURL,
                        DbModel.COLUMN_SUMMARY, DbModel.COLUMN_RATE, DbModel.COLUMN_URL},
                DbModel.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        DbModel model = new DbModel(
                cursor.getInt(cursor.getColumnIndex(DbModel.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(DbModel.COLUMN_URL)),
                cursor.getString(cursor.getColumnIndex(DbModel.COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(DbModel.COLUMN_SUMMARY)),
                cursor.getString(cursor.getColumnIndex(DbModel.COLUMN_ORIGINALURL)),
                cursor.getString(cursor.getColumnIndex(DbModel.COLUMN_RATE))
        );

        // close the db connection
        cursor.close();

        return model;
    }

    public List<DbModel> getAllMovies() {
        List<DbModel> movies = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DbModel.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DbModel movie = new DbModel();
                movie.setId(cursor.getInt(cursor.getColumnIndex(DbModel.COLUMN_ID)));
                movie.setUrl(cursor.getString(cursor.getColumnIndex(DbModel.COLUMN_URL)));
                movie.setName(cursor.getString(cursor.getColumnIndex(DbModel.COLUMN_NAME)));
                movie.setSummary(cursor.getString(cursor.getColumnIndex(DbModel.COLUMN_SUMMARY)));
                movie.setOriginalurl(cursor.getString(cursor.getColumnIndex(DbModel.COLUMN_ORIGINALURL)));
                movie.setRate(cursor.getString(cursor.getColumnIndex(DbModel.COLUMN_RATE)));

                movies.add(movie);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return movies;
    }

    public int getMoviesCount() {
        String countQuery = "SELECT  * FROM " + DbModel.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public void deleteMovie(DbModel movie) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DbModel.TABLE_NAME, DbModel.COLUMN_ID + " = ?",
                new String[]{String.valueOf(movie.getId())});
        db.close();
    }
}
